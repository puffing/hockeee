package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"

	"gitlab.com/puffing/hockeee/internal/m3u8api"
	"gitlab.com/puffing/hockeee/internal/nhlapi"
	"gitlab.com/puffing/hockeee/internal/proxy"
	"gitlab.com/puffing/hockeee/internal/utils"

	"github.com/rgeoghegan/tabulate"
)

func playGame(mediaPlaybackID string) {
	url, err := m3u8api.GetM3u8URL(mediaPlaybackID)
	if err != nil {
		log.Fatal(err)
	}

	port, err := proxy.RunProxyServer()
	if err != nil {
		log.Fatal(err)
	}

	args := []string{
		fmt.Sprintf(`--http-proxy=http://127.0.0.1:%d`, port),
		fmt.Sprintf(`--https-proxy=http://127.0.0.1:%d`, port),
		`--player=mpv`,
		fmt.Sprintf(`--http-cookie=mediaAuth=%s`, utils.GetRandomString(240)),
		fmt.Sprintf(`--http-header=User-Agent=%s`, utils.GetRandomUserAgent()),
		fmt.Sprintf(`hls://%s name_key=bitrate`, url),
		`best`,
		`--http-no-ssl-verify`,
	}

	cmd := exec.Command("streamlink", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	log.Fatal(cmd.Wait())
}

func displaySchedule() {
	schedule, err := nhlapi.GetSchedule()
	if err != nil {
		log.Fatal(err)
	}

	for _, game := range schedule.Dates[0].Games {
		date := game.Date.Local().Format("15:04:05")
		fmt.Printf("Game: %s at %s at %s\n\n", game.Teams.Away.Team.Abbrev, game.Teams.Home.Team.Abbrev, date)

		for _, epg := range game.Content.Media.EPG {
			if epg.Platform == "web" {
				var rows [][]string

				for _, item := range epg.Items {
					rows = append(rows, []string{
						item.MediaFeedType,
						item.CallLetters,
						item.MediaPlaybackID,
						fmt.Sprint(item.MediaState == "MEDIA_ON"),
					})
				}

				text, _ := tabulate.Tabulate(rows, &tabulate.Layout{
					Format:      tabulate.SimpleFormat,
					HideHeaders: false,
					Headers:     []string{"Type", "Network", "Game ID", "On"},
				})
				fmt.Println(text)
			}
		}

		fmt.Println("----------------------")
		fmt.Println("")
	}
}

func main() {
	play := flag.String("play", "", "Media ID to play")
	flag.Parse()

	if play != nil && len(*play) > 0 {
		playGame(*play)
	} else {
		displaySchedule()
	}

	return

}
