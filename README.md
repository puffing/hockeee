# hockeee
Reverse engineered from: https://github.com/NHLGames/NHLGames/
And: https://github.com/jwallet/go-mlbam-proxy

# Installation
`hockeee` depends on `streamlink` and `mpv`. You can install `streamlink` with pip and `mpv` from your Linux
distribution's repositories. Make sure `streamlink` and `mpv` are in your `PATH` variable.

You can build `hockeee` with `make build`. You need to have a Go 1.13+ compiler installed. On Debian Buster you can
find Go 1.14 in the `buster-backports` repos.

# Usage
`./hockeee` display the list of games for today.

Output will look like

```
CHI at EDM
NATIONAL   SN   2000008231 ON=true
AWAY   NBC   2000008241 ON=true
COMPOSITE      2000008291 ON=true

...
```

Then play the game & feed you want by using the stream ID.

`./hockeee --play 2000008231`

Right now there's only support for playing the best quality. But more to come!
