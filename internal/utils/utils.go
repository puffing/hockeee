package utils

import (
	"math/rand"
	"strings"
)

const loweralphanum = "abcdefghijklmnopqrstuvwxyz0123456789"

var userAgents = []string{
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:79.0) Gecko/20100101 Firefox/79.0",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36",
	"Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko",
	"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.49",
}

// GetRandomString returns a random string of the given length.
func GetRandomString(length int) string {
	var b strings.Builder
	for i := 0; i < length; i++ {
		idx := rand.Intn(len(loweralphanum))
		b.WriteByte(loweralphanum[idx])
	}
	return b.String()
}

// GetRandomUserAgent returns a random user agent.
func GetRandomUserAgent() string {
	idx := rand.Intn(len(userAgents))
	return userAgents[idx]
}
