package m3u8api

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

var m3u8ServerList = []string{"freegamez.ga", "freesports.ddns.net"}

func GetM3u8URL(playbackID string) (string, error) {
	loc, err := time.LoadLocation("America/Vancouver")
	if err != nil {
		panic(err)
	}
	today := time.Now().In(loc).Format("2006-01-02")
	url := fmt.Sprintf("http://%s/getM3U8.php?league=NHL&id=%s&cdn=%s&date=%s", m3u8ServerList[0], playbackID, "akc", today)

	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("m3u8api: request error: %v", resp.StatusCode)
	}

	buf, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", nil
	}

	return string(buf), nil
}
