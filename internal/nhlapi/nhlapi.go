package nhlapi

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/puffing/hockeee/internal/utils"
)

const apiURL = "https://statsapi.web.nhl.com/api/v1/schedule"

func GetSchedule() (*Schedule, error) {
	today := time.Now().Format("2006-01-02")
	reqURL, _ := url.Parse(apiURL)
	query := reqURL.Query()
	query.Set("startDate", today)
	query.Set("endDate", today)
	query.Set("expand", "schedule.teams,schedule.linescopre,schedule.game.seriesSummary,schedule.game.content.media.epg")
	reqURL.RawQuery = query.Encode()

	req, err := http.NewRequest("GET", reqURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", utils.GetRandomUserAgent())

	// TODO add cookie jar for random session

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	schedule := &Schedule{}
	if err := json.NewDecoder(resp.Body).Decode(schedule); err != nil {
		return nil, err
	}

	return schedule, nil
}
