package nhlapi

import "time"

type Schedule struct {
	Dates []*Date `json:"dates"`
}

type Date struct {
	Date  string  `json:"date"`
	Games []*Game `json:"games"`
}

type Game struct {
	GamePK  uint64      `json:"gamePk"`
	Date    time.Time   `json:"gameDate"`
	Teams   GameTeams   `json:"teams"`
	Content GameContent `json:"content"`
}

type GameTeams struct {
	Away *GameTeam `json:"away"`
	Home *GameTeam `json:"home"`
}

type GameTeam struct {
	Team *Team `json:"team"`
}

type Team struct {
	Name   string `json:"name"`
	Abbrev string `json:"abbreviation"`
}

type GameContent struct {
	Media *Media `json:"media"`
}

type Media struct {
	EPG []*EPG `json:"epg"`
}

type EPG struct {
	Title    string     `json:"title"`
	Platform string     `json:"platform"`
	Items    []*EPGItem `json:"items"`
}

type EPGItem struct {
	GUID            string `json:"guid"`
	MediaState      string `json:"mediaState"`
	MediaPlaybackID string `json:"mediaPlaybackID"`
	MediaFeedType   string `json:"mediaFeedType"`
	CallLetters     string `json:"callLetters"`
	Language        string `json:"language"`
}
