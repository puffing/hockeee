module gitlab.com/puffing/hockeee

go 1.13

require (
	github.com/rgeoghegan/tabulate v0.0.0-20170527210506-f65c88667bb4
	github.com/stretchr/testify v1.6.1 // indirect
)
